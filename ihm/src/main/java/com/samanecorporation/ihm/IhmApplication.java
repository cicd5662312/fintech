package com.samanecorporation.ihm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan({"com.samanecorporation.metier.sevice", "com.samanecorporation.metier.mapper", "com.samanecorporation.ihm.controller"})
@EntityScan("com.samanecorporation.metier.entity")
@EnableJpaRepositories("com.samanecorporation.metier.dao")
public class IhmApplication {

	public static void main(String[] args) {
		SpringApplication.run(IhmApplication.class, args);
	}

}
