package com.samanecorporation.metier.dao;

import com.samanecorporation.metier.entity.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<AccountEntity, String> {
    Optional<AccountEntity> findByNumber(String number);
    Optional<AccountEntity> findByCurrency(String currency);
}
