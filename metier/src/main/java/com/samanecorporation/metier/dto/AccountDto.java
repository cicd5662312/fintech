package com.samanecorporation.metier.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class AccountDto {

    private String number;
    private double amount;
    private String currency;
}
