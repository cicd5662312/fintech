package com.samanecorporation.metier.mapper;

import com.samanecorporation.metier.dto.AccountDto;
import com.samanecorporation.metier.entity.AccountEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface AccountMapper {

    AccountDto toAccountDto(AccountEntity accountEntity);
    AccountEntity toAccountEntity(AccountDto accountDto);
    List<AccountDto> toAccountDtoList(List<AccountEntity> accountEntityList);
}
