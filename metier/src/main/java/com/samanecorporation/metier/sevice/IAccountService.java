package com.samanecorporation.metier.sevice;

import com.samanecorporation.metier.dto.AccountDto;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface IAccountService {
    Optional<AccountDto> save(AccountDto accountDto);
    Optional<AccountDto> get(String number);
    List<AccountDto> findAll();
}
